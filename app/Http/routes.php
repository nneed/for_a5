<?php

use App\Page;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => 'web'], function () {

    /**
     * Show Page 
     */
	Route::get('/', function () {
	    return view('pages');
	});

    /**
     * Add New Page
     */
    Route::post('/page', function (Request $request) {
        //
    });

    /**
     * Delete Page
     */
    Route::delete('/page/{page}', function (Page $page) {
        //
    });


});